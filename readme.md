# LANDLAB Public website

## This is the LANDL@B public repository. 



Because of their importance in erosion and sediment supply in rivers, hill-slope processes are among the most essential geophysical mechanisms that drive the evolution of the Earth's surface. By allowing the percolation of surface runoff, they create conditions for chemical weathering, which are an important atmospheric carbon sink.


The LandLab project aims to assess the role of hill-slope processes in sediment and solute fluxes in small watersheds. Through an approach combining in-situ measurements, geochemical analysis and remote sensing at multiple scales, LandLab will allow a stepwise analysis between hill-slope processes and sediment load in rivers.  

To do so, we aim at developing autonomous boxes which will drive a turbidimeter, a pressure sensor and an optical sensor for monitoring river fluxes and sediment charges. We need a Jetson system in order to drive those instruments which will be deployed over 4 watersheds experiencing both tropical and alpine climate.

![](LandLab.png)



We acknowledge the support from the French space agency CNES, the Nvidia company and our host institute IPGP. 
